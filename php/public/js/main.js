(function(){

    document.querySelector('form').addEventListener('submit',function(ev){
        ev.preventDefault();
        validate();
    })

    document.querySelector('#startdate').addEventListener('change', setStartdate)
    document.querySelector('#enddate').addEventListener('change', setEnddate)

    document.querySelector('#startdate').addEventListener('focus', displayError(false))
    document.querySelector('#enddate').addEventListener('focus', displayError(false))

    document.querySelector('#baseline').addEventListener('input', checkBaseline)
    document.querySelector('#total').addEventListener('input', checkTotal)

})()



var globalVal=true;

function setStartdate(ev){
    var startdate=ev.target;
    var selectedDate = new Date(startdate.value)
   
    var today =new Date();
    if(today>selectedDate){
        displayError(true)
        globalVal=false
    }else{
        displayError(false)
         globalVal = true
        var enddate=document.querySelector('#enddate')
        enddate.min=startdate.value;
    }
    
}

function setEnddate(ev) {
    var enddate = ev.target;
    var startdate = document.querySelector('#startdate')

    if(enddate.value<startdate.value){
        displayError(true)
         globalVal = false
    }
    else{
        displayError(false)
    }
}


function displayError(par) {
    if (par){
        document.querySelector('#error').style.visibility='visible'
    }
    else{
        document.querySelector('#error').style.visibility = 'hidden'
    }
}


function checkBaseline (ev) {
    if (ev.target.value > document.querySelector('#total').value)
        ev.target.max = document.querySelector('#total').value;

}


function checkTotal(ev){
    document.querySelector('#baseline').max=ev.target.value;

}

function validate() {
 var data= new FormData(document.querySelector('form'))
 data.append('action','random')
 
      $.ajax({
          type: "POST",
          url: 'server/index.php',
          data: data,
          dataType: 'text',
          contentType: false,
          processData: false,

          success: parseData,
          fail: function (err) {
              console.log('erreur')
          }
      });
 


}

function parseData(dt,st){
    var res = JSON.parse(dt);
    RenderResult(res);
}

function RenderResult(arr) {
    var keys=Object.keys(arr);
    var lg=keys.length;
    var content = `
        <ul class="list-group mb-0 m-0">
            <div class = "col-12 border shadow rounded-top text-center p-5 bg-success"
             style = "font-size:3vmin; color:white; font-weight: bold" >
                Total Working days: `+ lg+` days
            </div>
    
        `
    for(var i = 0 ; i < lg ; i++){
        content+=`
                <li class = "list-group-item d-flex justify-content-between align-items-center p-0" >
                    <p class = "col-8 d-flex border border-info h-100 rounded-left justify-content-center align-items-center bg-info" >
                    `+ keys[i]+` 
                    </p> 
                    <p class = "col-4 d-flex border border-info h-100 rounded-right justify-content-center align-items-center bg-light " >
                    `+ arr[keys[i]] +`
                    </p> 
                 </li>
        `
    }

    content += `
            </ul>
             <button class = "btn border border-danger bg-danger col-12 text-light m-0" onclick="resetAll()" > reset </button>
            
            `;

    document.querySelector('#result').innerHTML = content;
}


function resetAll(){

    document.querySelector('#result').innerHTML = '';
    document.querySelector('#startdate').value=new Date();
    document.querySelector('#enddate').value = new Date();
    document.querySelector('#total').value=1
    document.querySelector('#baseline').value = 1
    
}

