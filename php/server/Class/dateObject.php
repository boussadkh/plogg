<?php
class dateObject
{

    function __constructor()
    {

    }

    static function generateArray($nbdays, $startdate)
    {
        $arr = array();
        $st = $startdate;
        $i = 0;
        //echo $nbdays;
        $item = $st;

        do {
            $d4 = (int)date_format($item, 'N');
            if ($d4 != 6 && $d4 != 7) {

                $arr[date_format($item, 'Y-m-d')] = 0;

            }

            $item = date_modify($st, '+1 day');
            ++$i;
        } while ($i < $nbdays);

        return $arr;


    }




    static function InitBaseline($arr, $basis)
    {
        $sum=0;
        $nbitems = count($arr);
        $fract = number_format($basis / $nbitems, 2, '.', ' ');

        foreach ($arr as $key => $value) {
            $arr[$key] = $fract;
        }

        foreach ($arr as $key => $value) {
            $sum+=$arr[$key];
        }
        return [$sum ,$arr];
    }




    static function randomDistribution($arr , $todistribute){
        $remain= $todistribute;
        $sp=0;
        if ($todistribute<=0)
        return $arr;
        do{
            foreach($arr as $key=>$value){
                $nbr=rand(0, $remain *100)/100;
                $arr[$key]+=$nbr;

                $remain =abs($remain -$nbr);
                $sp+= $nbr;
            }
            
        }while((abs($todistribute-$sp)/$todistribute)>0.01);
        return $arr;


    } 


}

?>
