<?php
require 'dateObject.php';
class formater
{

    function __construct()
    {

    }
    private $total;
    private $baseline;
    private $startdate;
    private $enddate;
    private $totalDays;
    private $generatedArray;

    public function Init($obj)
    {
        $this->total = isset($obj['total']) ? $obj['total'] : null;        
        $this->baseline = isset($obj['baseline']) ? $obj['baseline'] : null;
        $this->startdate = isset($obj['startdate']) ? $obj['startdate'] : null;
        $this->enddate = isset($obj['enddate']) ? $obj['enddate'] : null;


        $d1= date_create($this->startdate);
        $d2 = date_create($this->enddate);

        $this->totalDays=1+(int) date_diff($d1, $d2)->days;

        $this->generatedArray= dateObject::generateArray($this->totalDays, $d1);

        $baseArray= dateObject::InitBaseline($this->generatedArray,$this->baseline);

        $this->generatedArray = $baseArray[1];
        $this->total-= $baseArray[0];

        $this->generatedArray=dateObject::randomDistribution($this->generatedArray,$this->total);
    
        echo json_encode($this->generatedArray);

    }

    


}
?>